# Coding challenge : ITWM Fraunhofer quantum group 

Welcome to the programming challenge of our group ! The puprose of those challenges are to measure you habilities to solve and program simple scientific codes. It is farily easy, you have however time to finish it properly. You are being given two files

1. ```src/discrete.py``` 
2. ```coding_challenge.py```

You will have to modify/correct/debug the first one in one hand, on the other hand you have to bring your own implementation in the second one. Good luck, and have fun !


Ho, no...


I completely forgot about Isacco's birthday, AGAIN ! How am I going out of this situation without having to deal with silence treatment for a good month this time ? I have some idea, hopefully with your help, I might get out of this risky situation. 

Last week I was rambling about dynamical unstable systems displaying chaotic behavior and how interesting it was that they map to a projection of fractal systems. Isacco responded with "sure man" and other very excited "ho well...". With such explosion of excitement and interest it is clear to me that the perfect gift would be a good print of either a bifurcation graph or some beautiful attractor representation (not exceeding of course the 25 euros anti-corruption policy at Fraunhofer-Gesellschaft). I looked at the wikipedia pageof the Hénon map and thought it was perfect https://en.wikipedia.org/wiki/H%C3%A9non_map. 

If you know nothing of what I just said, fear not as I wrote the progam myself, however I did it in an hurry and wrote hastily some basic math, while the actual programming part was left to the reader. I probably did a lot of mistakes but the overall logic seems fine to me. 

We are expecting the following tasks to be fullfilled: 

1. Basics
    1. Get the code of the challenge and run it in python 3.2 minimum. All required libraries are available in the requirement.txt file (hint: maybe there is a way to install those libraries in one command given the file to not waste time ?). In any case it's only the graphical library so it shouldn't be too hard anyways. It is adviced to work in a virtual environment like ```Anaconda```, it's always more careful.
    2. Import all the functions in the ```src``` folder
    3. Debug the code until there is not error left. 
    4. Run the code and understand it. 

2. Personal implementation 
    1. Implement the missing functions according to the requirements 
    2. I need to work of art
        a) The scatter plot according to requirements
        b) Divergence graph with many experiments, varying the alpha parameter. Can we vary it in any way we want ?
        c) I can probably make the divergence plot look great if there's a way to change the alpha depending on the probability of one point to be at a particular place 
           I wrote some density function and I think the parameters are right so it looks good. Just have to vary the plot correctly. 
    3. Personal contribution is a bonus, I think it would be very impressive to make a small video of either the evolution of the map, or it's creation ! I think it's quite easy using ```ffmpeg``` in some bash script. 