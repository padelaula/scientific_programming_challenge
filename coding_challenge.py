"""
Author : Théo Lisart
    In case of issues please contact : theo.lisart@itwm.fraunhofer.de

Vers: 0.1

╔══╗╔════╗╔╗╔╗╔╗╔═╗╔═╗     ╔═══╗                ╔╗       ╔═╗           ╔══╗         ╔╗    ╔╗      ╔╗     
╚╣╠╝║╔╗╔╗║║║║║║║║║╚╝║║     ║╔══╝                ║║       ║╔╝           ╚╣╠╝        ╔╝╚╗  ╔╝╚╗    ╔╝╚╗    
 ║║ ╚╝║║╚╝║║║║║║║╔╗╔╗║     ║╚══╗╔═╗╔══╗ ╔╗╔╗╔═╗ ║╚═╗╔══╗╔╝╚╗╔══╗╔═╗     ║║ ╔═╗ ╔══╗╚╗╔╝╔╗╚╗╔╝╔╗╔╗╚╗╔╝╔══╗
 ║║   ║║  ║╚╝╚╝║║║║║║║     ║╔══╝║╔╝╚ ╗║ ║║║║║╔╗╗║╔╗║║╔╗║╚╗╔╝║╔╗║║╔╝     ║║ ║╔╗╗║══╣ ║║ ╠╣ ║║ ║║║║ ║║ ║╔╗║
╔╣╠╗ ╔╝╚╗ ╚╗╔╗╔╝║║║║║║    ╔╝╚╗  ║║ ║╚╝╚╗║╚╝║║║║║║║║║║╚╝║ ║║ ║║═╣║║     ╔╣╠╗║║║║╠══║ ║╚╗║║ ║╚╗║╚╝║ ║╚╗║║═╣
╚══╝ ╚══╝  ╚╝╚╝ ╚╝╚╝╚╝    ╚══╝  ╚╝ ╚═══╝╚══╝╚╝╚╝╚╝╚╝╚══╝ ╚╝ ╚══╝╚╝     ╚══╝╚╝╚╝╚══╝ ╚═╝╚╝ ╚═╝╚══╝ ╚═╝╚══╝                                                                                      
                                                                                             
▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
██ ██ ██▄██ ▄▄▀██▄██ ▄▄▀█ ▄▄▄███▀▄▀█▀▄▄▀█ ▄▀██▄██ ▄▄▀█ ▄▄▄███▀▄▀█ ████ ▄▄▀█ ██ ██ ▄▄█ ▄▄▀█ ▄▄▄█ ▄▄
██ ▄▄ ██ ▄█ ▀▀▄██ ▄█ ██ █ █▄▀███ █▀█ ██ █ █ ██ ▄█ ██ █ █▄▀███ █▀█ ▄▄ █ ▀▀ █ ██ ██ ▄▄█ ██ █ █▄▀█ ▄▄
██ ██ █▄▄▄█▄█▄▄█▄▄▄█▄██▄█▄▄▄▄████▄███▄▄██▄▄██▄▄▄█▄██▄█▄▄▄▄████▄██▄██▄█▄██▄█▄▄█▄▄█▄▄▄█▄██▄█▄▄▄▄█▄▄▄
▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀
"""

# Internal math toolbox

# Tooling imports 
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import src.discrete as disc

# Numerical parameters 
MAXIT, MAXA, step_a = 1000, 1.42, 0.005   # The smallest the step_a, the longuest it computes, must find a good value such a way it doesn't take all night, but still looks good !

# Experiment parameters
beta, alpha = 0.3, 1.4
x0, y0 = 0.7, 0.7  # Initial conditions, chaotic systems are very sensitive to this, sometimes it might just crash, better be careful and look for the right values

# Animation settings
video_file = "map_video.mp4"
clear_frames = False
fps = 13


def scatterPlot(x, y, title):
    """
    Desc: Simple plot calling Matplotlib library, plotting the data as a scatterplot, removes the thicks and adds the title 

    INPUT:         x --> position argument (list)
                   t --> time argument (list)
    OUTPUT : Void  
    """

    plt.scatter(x, y, color='black', s=2)
    plt.xticks([])
    plt.yticks([])
    plt.title(title)
    plt.show()


def orbitPlot(a, x, title):
    """ 
    Desc: Go through the experiments data and plots everything per alpha steps. Necessary step to have data size compatibility in matplotlib. 

    INPUT:   a --> alpha array to be plotted on the x-axis (list)
             x --> experiments results associated to it    (list of list)

    OUTPUT:  Void
    """

    for a1, x1 in zip(a, x):
        dist = disc.normalizedDensity(x1)
        plt.scatter([a1] * len(x1), x1, color='black', marker=".", s=1, alpha=dist)
    plt.axis('off')
    plt.title(title)
    plt.show()


def mapAnimation(a, x, title):
    ffmpeg_writer = animation.writers['ffmpeg']
    metadata = dict(title='Movie Map', artist='Davide', comment='Enjoy!')
    writer = ffmpeg_writer(fps=fps, metadata=metadata)

    fig = plt.figure()
    plt.axis('off')
    plt.title(title)
    with writer.saving(fig, video_file, 100):
        for a1, x1 in zip(a, x):
            if clear_frames:
                fig.clear()
            dist = disc.normalizedDensity(x1)
            plt.scatter([a1] * len(x1), x1, color='black', marker=".", s=1, alpha=dist)
            writer.grab_frame()


def main():
    """
    Ok don't panic, what we have to do : 
        1) Make this whole thing work 
        2) Make two plots from this math thing
            a) One plot with the attractor with written as title "You are this cool", WITH ONLY THE FRAME: no thick,
             no label, no axi
            b) One plot with the divergence graph with title "Happy birthday Isacco, may the Mountain be on your side",
             with NO FRAME, no nothing

            In both case those are scatterplots, with black dots, for the b) I guess i can make something pretty by
            varying the alphas (as in transparency, not the variable we used) according to the
            disc.normalizedDensity(vector) function. Good I wrote this function in the past it seems almost good to go. 
    """

    # a)
    x, y = disc.henonMapLogisticFunction(x0, y0, alpha, beta, MAXIT)
    scatterPlot(x, y, "You are this cool")
    #
    # # b)
    data, a = disc.henonMapOrbitDiagram(x0, y0, beta, MAXA, MAXIT, step_a)
    orbitPlot(a, data, "Buon compleanno Isacco, possa la Montagna essere al tuo fianco :)")

    # c)
    # animation of b) using ffmpeg (needs to be installed in the system)
    data, a = disc.henonMapOrbitDiagram(x0, y0, beta, MAXA, MAXIT, step_a)
    mapAnimation(a, data, "Icing on the cake")


# Running the experiments 
main()
