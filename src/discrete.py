"""
File description : 
    Numerical analysis toolbox. 

Author : Théo Lisart
    In case of issues please contact : theo.lisart@itwm.fraunhofer.de

Vers: 0.1
"""
# define Python user-defined exceptions


class NegativeIterationError(Exception):
    "Raised when the iterazion value is less than 0"
    pass


def henonMapLogisticFunction(x0, y0, alpha, beta, MAXIT):
    """
    Desc: Logistic functions are recurrent expressions appearing in a large range of physics and complex system modelling. Interestingly, they present chaotic behavior and convergence 
          points are essential in many area of optimization of complex systems. It is the most studied chaotic system. 

          Classic values : beta = 0.3
                           alpha = 1.4

          We define the Hénon map as the two-parameter logistic function 

            x_{t+1} = 1 - \alpha x_t + \beta x_{t -1}

          From this expression it is clear that two initial conditions have to be given. 

    INPUT: x0    --> initial x position          (float)
           y0    --> initial y position          (float)
           alpha --> first quadratic coefficient (float)
           beta  --> linear coefficient          (float)
           MAXIT --> Maximum iteration           (int)

    OUTPUT: x  --> x array set of points (list)
            y  --> y array set of points (list)
    """

    try:
        x, y = [x0], [y0]

        if MAXIT < 0:
            raise NegativeIterationError()

        for i in range(MAXIT):
                x.append(1 - alpha*(x[i]**2) + y[i])
                y.append(beta*x[i])
    except OverflowError:
        print("Overflow error in mapping of points in logistic function")

    except TypeError:
        print("Type erro in logistic function. MAXIT must be int")

    except NegativeIterationError:
        print("Index error in logistic function computation. MAXIT cannot be negative")
        
    return x, y


def henonMapOrbitDiagram(x0, y0, beta, MAXA, MAXIT, step_a):
    """
    Desc: Creates the data necessary to build the orbital map of the Hénon attractor function. 
          Some experiments shows stable orbits when other diverge, when other oscillate, other display chaotic behavior. 

    INPUT: x0     --> initial x position            (float)
           y0     --> initial y position            (float)
           beta   --> linear coefficient            (float)
           MAXA   --> Maximum alpha value           (float)
           MAXIT  --> Maximum iteration             (int)
           step_a --> Computational timestep \alpha (float)

    OUTPUT: data  --> data points per alpha values (list of list)
            a     --> alpha values for each run    (list)

    """

    a, data = [], []
    alpha = 0.0
    
    while alpha < MAXA:

        x, y = henonMapLogisticFunction(x0, y0, alpha, beta, MAXIT)
        data.append(x)
        a.append(alpha)
        alpha += step_a
        
    return data, a


def normalizedDensity(vector):
    """ 
    Desc: pseudo - density normalized value per vector entries. Associate a value between 0 and 1 depending on the
         probability to have a point for a given alpha value. Adjusted for eastetic reasons.

    INPUT: vector    --> Data column vector        (list)
    OUTOUT: distance --> density value as distance (float) 
    """ 

    minValue, maxValue, density = 0, 0, 0

    try:
        for i in range(len(vector)):
            buffValue = vector[i]

            for j in range(len(vector)):
                if (vector[j] > buffValue):
                    maxValue = vector[j]

                elif (vector[j] < buffValue):
                    minValue = vector[j]

        distance = ((maxValue - minValue)/len(vector)) * 10 + 0.1
        return distance

    except ZeroDivisionError:
        print("Error zero division in normalDensity function")

    # is it really necessary?
    except IndexError:
        print("Error index Error in normalDensity function")
